import scalaj.http.Http
import io.circe.parser.parse
import io.circe.{Decoder, HCursor, Json, Encoder}

import java.io._
import java.net.{InetAddress, ServerSocket, Socket, SocketException}
import java.util.Random

object Meteo {

  def KelvinToCelsius(kelvin: Double): Double = {
    val rep: Double = kelvin - 273.15

    return rep - (rep % 0.1)
  }

  def getMeteo(lat: Double, lon: Double): String = {

    val v=Map[String,String] ("lat"->lat.toString,"lon"->lon.toString,"appid"->"9a238a545bece80bca533e32bcd25588")
    val response: String = Http("http://api.openweathermap.org/data/2.5/weather").params(v).asString.body

    println(response)

    val doc: Json = parse(response).getOrElse(Json.Null)
    val cursor: HCursor = doc.hcursor

    val long: Decoder.Result[Double] = cursor.downField("coord").downField("lon").as[Double]
    println(long)
    val lati: Decoder.Result[Double] = cursor.downField("coord").downField("lat").as[Double]
    println(lati)

    val descritpionR: String = cursor.downField("weather").downN(0).downField("description").as[String].toString
    println(descritpionR)
    val description: String = descritpionR.slice(6,descritpionR.length-1)
    println(description)

    val tempR: String = cursor.downField("main").downField("temp").as[Double].toString
    println(tempR)
    var temp: Double = tempR.slice(6,tempR.length-1).toDouble
    temp = KelvinToCelsius(temp)
    println(temp)

    val cityR: String = cursor.downField("name").as[String].toString
    println(cityR)
    val city: String = cityR.slice(6,cityR.length-1)
    println(city)

    var rawJson = """{"Ville": """"

    rawJson+=city
    rawJson+="""", "Temperature": """"
    rawJson+=temp
    rawJson+="""", "Meteo": """"
    rawJson+=description
    rawJson+=""""}"""

    return parse(rawJson).getOrElse(Json.Null).toString()
  }
}

object listener {
  def main(args: Array[String]): Unit = {
    try {
      val listener = new ServerSocket(9999);
      while (true)
        new ServerThread(listener.accept()).start();
      listener.close()
    }
    catch {
      case e: IOException =>
        System.err.println("Could not listen on port: 9999.");
        System.exit(-1)
    }
  }
}

case class ServerThread(socket: Socket) extends Thread("ServerThread") {
  override def run(): Unit = {
    try {
      val out = new ObjectOutputStream(socket.getOutputStream());
      val in = new ObjectInputStream(
        new DataInputStream(socket.getInputStream()));

      val filter = in.readObject().asInstanceOf[Array[Double]];
      val resp = Meteo.getMeteo(filter(0), filter(1))

      out.writeObject(resp)

      Thread.sleep(100)

      out.close();
      in.close();
      socket.close()
    }
    catch {
      case e: SocketException =>
        () // avoid stack trace when stopping a client with Ctrl-C
      case e: IOException =>
        e.printStackTrace();
    }
  }
}