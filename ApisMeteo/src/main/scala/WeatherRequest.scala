import io.circe.parser.parse
import io.circe.{Decoder, HCursor, Json}

import scalaj.http.Http

class WeatherRequest(lat: String, lon: String) {
  def getLat = lat
  def getLon = lon

  def getWeather(): String = {
    val requestParams = Map[String,String] ("lat"->lat,"lon"->lon,"appid"->"9a238a545bece80bca533e32bcd25588")
    val request = Http("http://api.openweathermap.org/data/2.5/weather").params(requestParams).asString.body
    return request
  }

  def printRequest(req: String): String = {
    val doc: Json = parse(req).getOrElse(Json.Null)
    val cursor: HCursor = doc.hcursor
    println(req)
    val lon: Decoder.Result[Double] = cursor.downField("coord").downField("lon").as[Double]
    println(lon)
    val lat: Decoder.Result[Double] = cursor.downField("coord").downField("lat").as[Double]
    println(lat)
    val descritpion: Decoder.Result[String] = cursor.downField("weather").get("id")as[String]
      println(descritpion)
    val main: Decoder.Result[Double] = cursor.downField("main").downField("temp").as[Double]
    println(main)
  }
}
