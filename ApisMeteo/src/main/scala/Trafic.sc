import io.circe.parser.parse
import io.circe.{Decoder, HCursor, Json}

import scalaj.http.Http

class TraficRequest(lat: Double, long: Double) {

  def getNorth(lat : Double, long : Double) : Double = {
    val north = lat + 1.5 * 360 / 40000
    return north
  }

  def getSouth(lat : Double, long : Double) : Double = {
    val south = lat - 1.5 * 360 / 40000
    return south
  }

  def getEst(lat : Double, long : Double) : Double = {
    val est = long + 1.5 / (Math.cos(Math.PI * lat / 180) * 111.11)
    return est
  }

  def getWest(lat : Double, long : Double) : Double = {
    val west = long - 1.5 / (Math.cos(Math.PI * lat / 180) * 111.11)
    return west
  }

  def getMeteo() {

    val response = Http("http://dev.virtualearth.net/REST/v1/Traffic/Incidents/" + getSouth(this.long,this.lat) +","+ getWest(this.lat,this.long) +","+ getNorth(this.lat,this.long) +","+ getEst(this.lat,this.long) + "?key=ArpWkf7SAe7scxKa7ze6OXrmmmeIHW00CGLrGsCp1WvDJVBbNxdBRY7-Sph_CE_h").asString.body

    println(response)

    val doc: Json = parse(response).getOrElse(Json.Null)
    val cursor: HCursor = doc.hcursor

    val nbPoints: Decoder.Result[Int] = cursor.downField("resourceSets").downN(0).downField("estimatedTotal").as[Int]
    println(nbPoints)

    //for(i <- 0 to n-1){
    var lat: Decoder.Result[Double] = cursor.downField("ressourceSets").downN(0).downField("resources").downN(0).downField("point").downField("coordinates").downN(0).as[Double]
    println(lat)

    var long: Decoder.Result[Double] = cursor.downField("ressourceSets").downN(0).downField("resources").downN(0).downField("point").downField("coordinates").downN(1).as[Double]
    println(long)
    //}
  }



}