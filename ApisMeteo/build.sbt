name := "ApisMeteo"

version := "0.1"

scalaVersion in ThisBuild := "2.12.4"

libraryDependencies +=  "org.scalaj" %% "scalaj-http" % "2.3.0"

libraryDependencies += "com.trueaccord.scalapb" %% "scalapb-json4s" % "0.3.2"

val circeVersion = "0.8.0"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)
