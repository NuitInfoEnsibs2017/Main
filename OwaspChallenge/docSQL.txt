Explication de la faille :

L'injection SQL est une méthode d'injection permettant de récupérer des données depuis une base de données.

L'attaquant va profiter d'une faille dans le code de la page pour envoyer une requête SQL.
Cette requête va être interprétée par le système de gestion de base de données. Elle va ensuite retourner les données demandées dans la requête de l'attaquant. 
Nous pouvons envisager une injection d'une simple requête pour visualiser les données de la base. Nous pouvons également imaginer une injection d'une commande de création ou de modification de table donnant ainsi accès à l'ensemble de la base de données à l'attaquant. Ce dernier est alors libre de modifier l'intégrité des données présentes dans la base de données.

Exemple :

Injection simple pour se connecter en admin : '' OR 1=1 --


Corriger la faille :

La fonction mysql_real_escape_string va permettre de contrôler les champs entrés de la présence de caractères spéciaux , elle renvoie FALSE si une erreur survient.
