import scala.collection.immutable.List
import scala.collection.mutable.MutableList


trait Observer { // Subscriber
  def notification()
}

// Concrete Implementation

class SomeObservable extends Observable

