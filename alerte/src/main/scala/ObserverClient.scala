object ObserverClient extends App {
  var observable = new SomeObservable()
  var observer1 = new Alerte("accident","nique mok","chez ta maman")
  println(observer1.typeAlerte)
  var observer2 = new AnotherObserver()
  //observable.addObserver(observer1)
  observable.addObserver(observer2)
  observable.notifyObservers()

  observable.removeObserver(observer2)
  observable.notifyObservers()
}