import java.util.Calendar
import io.circe.Json

class Alerte(val type_alerte :String,val desc:String,val adr:String) {

  var typeAlerte: String = type_alerte
  var dateAlerte = Calendar.getInstance.getTime
  var description: String = desc
  var adresse: String = adr

   def formatJson():String= {
    val jsonFromFields = Json.fromFields(List(("type", Json.fromString(typeAlerte)),
      ("date", Json.fromString(dateAlerte.toString)), ("description", Json.fromString(description)), ("adresse", Json.fromString(adresse))))
    return jsonFromFields.toString()
  }
}
