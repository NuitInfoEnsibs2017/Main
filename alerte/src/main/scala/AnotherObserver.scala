class AnotherObserver extends Observer {
  override def notification() {
    println("do something here: AnotherObserver")
  }
}