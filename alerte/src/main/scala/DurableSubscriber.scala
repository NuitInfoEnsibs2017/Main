import javax.jms._
import org.apache.activemq.ActiveMQConnection
import org.apache.activemq.ActiveMQConnectionFactory
class DurableSubscriber (name:String){
  val activeMqUrl: String = ActiveMQConnection.DEFAULT_BROKER_URL
  val topicName: String = name
  val connection: Connection = new ActiveMQConnectionFactory(activeMqUrl).createConnection
}
object DurableSubscriber {
  val activeMqUrl: String = ActiveMQConnection.DEFAULT_BROKER_URL
  val topicName: String = "TOPIC_NAME"

  def main(args: Array[String]): Unit = {
    // Set up the connection, same as the producer, however you also need to set a
    // unique ClientID which ActiveMQ uses to identify the durable subscriber

    var subscriber :DurableSubscriber =new DurableSubscriber("TOPIC_NAME")
    subscriber.connection.setClientID("SomeClientID")
    subscriber.connection.start

    // We don't want to use AUTO_ACKNOWLEDGE, instead we want to ensure the subscriber has successfully
    // processed the message before telling ActiveMQ to remove it, so we will use CLIENT_ACKNOWLEDGE
    val session: Session = subscriber.connection.createSession(false, Session.CLIENT_ACKNOWLEDGE)

    // Register to be notified of all messages on the topic
    val topic: Topic = session.createTopic(topicName)
    val durableSubscriber: TopicSubscriber = session.createDurableSubscriber(topic, "Test_Durable_Subscriber")
    durableSubscriber.setMessageListener((message: Message) => {
      try {
        if (message.isInstanceOf[TextMessage]) {
          val textMessage: TextMessage = message.asInstanceOf[TextMessage]
          println("Message received from producer: '" + textMessage.getText + "'")
          // Once we have successfully processed the message, send an acknowledge back to ActiveMQ
          message.acknowledge
        }
      }
      catch {
        case je: JMSException => {
          println(je.getMessage)
        }
      }
    })

  }

}