import scala.collection.mutable.ListBuffer

trait Observable {//publisher
  var observers:ListBuffer[Observer] = ListBuffer()

  def notifyObservers() {
  //  for(observer <- observers)
   //   observer.notification()
    observers.foreach(obs => obs.notification())
  }

  def addObserver(observer:Observer) {
    observers += observer
  }

  def removeObserver(observer:Observer) {
    observers -= observer
  }
}
