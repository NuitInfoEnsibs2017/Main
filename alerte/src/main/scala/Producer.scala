import java.io.{DataInputStream, IOException, ObjectInputStream}
import java.net.{ServerSocket, Socket, SocketException}

import javax.jms._

import org.apache.activemq.ActiveMQConnection
import org.apache.activemq.ActiveMQConnectionFactory
class Producer (name:String){
  val activeMqUrl: String = ActiveMQConnection.DEFAULT_BROKER_URL
  val topicName: String = name
  val connection: Connection = new ActiveMQConnectionFactory(activeMqUrl).createConnection
}

  object listener {
    def main(args: Array[String]): Unit = {
      try {
        val listener = new ServerSocket(9999);
        while (true)
          new ServerThread(listener.accept()).start();
        listener.close()
      }
      catch {
        case e: IOException =>
          System.err.println("Could not listen on port: 9999.");
          System.exit(-1)
      }
    }
  }

  case class ServerThread(socket: Socket) extends Thread("ServerThread") {
    override def run(): Unit = {
      try {
        val in = new ObjectInputStream(
          new DataInputStream(socket.getInputStream()))

        val filter = in.readObject().asInstanceOf[Array[String]]
        var producer :Producer =new Producer(filter(0))
        producer.connection.start
        val session: Session = producer.connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
        var alerte: Alerte = new Alerte(filter(1), filter(2), filter(3))
        val destination: Destination = session.createTopic(producer.topicName)
        val messageProducer: MessageProducer = session.createProducer(destination)
        val textMessage: TextMessage = session.createTextMessage(alerte.formatJson())
        messageProducer.send(textMessage)
        println("Message sent to subscriber: '" + textMessage.getText + "'")
        producer.connection.close


        Thread.sleep(100)

        in.close()
        socket.close()
      }
      catch {
        case e: SocketException =>
          () // avoid stack trace when stopping a client with Ctrl-C
        case e: IOException =>
          e.printStackTrace();
      }
    }
  }

}