<?php

	function listPublicEvents() {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare( "SELECT * FROM Evenement WHERE publicEvenement = 1;" );
		$result = $con->query( $request );
		
		return json_encode($result);
	}
	
	function listPrivateEvents( $iduser ) {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare(
			"SELECT Evenement.*
			FROM Evenement, Groupe, Person
			WHERE Evenement.publicEvenement = 0
			AND Evenement.idEvenement = Groupe.parentEvenement
			AND Groupe.idGrp IN (
				SELECT groupe
				FROM ParticipantsGroupe
				WHERE person = ?
				UNION
				SELECT idGrp
				FROM Groupe
				WHERE leader = ?
				)
			UNION
			SELECT Evenement.*
			FROM Evenement
			WHERE orga = ?
			;"
		);
		$request->bind_param( 'iii', $iduser, $iduser, $iduser );
		$result = $con->query( $request );
		
		return json_encode($result);
	
	}
	
	function dataEvent( $idevent ) {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare( "SELECT * FROM Evenement WHERE idEvenement = ?" );
		$request->bind_param( 'i', $idevent );
		
		$result['eventData'] = $con->query( $request );
		
		$request->$con->prepare( "SELECT * FROM Publication WHERE evenement = ? " );
		$request->bind_param( 'i', $idevent );
		
		$result['eventDiscus'] = $con->query( $request );
		
		return json_encode($result);
	}
	
	function listGroupsFromEvent( $idevent ) {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare( "SELECT idGrp, nameGrp FROM Groupe WHERE idEvenement = ?" );
		$request->bind_param( 'i', $idevent );
		$subresult = $con->query( $request );
		
		while($row = $subresult->fetch_assoc()) {
			$result[ $row['nameGrp'] ] = listGuysFromGroup( $row['idGrp'] );
		}
		
		return json_encode($result);
	}
	
	function listGuysFromGroup( $idgroup ) {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare(
			"SELECT *
			FROM Person
			WHERE idPerson IN (
				SELECT leader
				FROM Groupe
				WHERE idGrp = ?
				UNION
				SELECT person
				FROM ParticipantsGroupe
				WHERE groupe = ?
			)
			;"
		);
		$request->bind_param( 'ii', $idgroup, $idgroup );
		$result = $con->query( $request );
		
		return json_encode($result);
	}
	
	function listAllMyGroups( $iduser ) {
		if( !isset($con) ) {
			include "connectBdd.php";
		}
		
		$request = $con->prepare(
			"SELECT *
			FROM Groupe
			WHERE leader = ?
			OR idGroup IN (
				SELECT groupe
				FROM ParticipantsGroupe
				WHERE person = ?
			)
			;"
		);
		$request->bind_param( 'ii', $iduser, $iduser );
		$result = $con->query( $request );
		
		return json_encode($result);
	}
	
?>
