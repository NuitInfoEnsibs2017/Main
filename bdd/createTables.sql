
drop table if exists SamGroupe;
drop table if exists ParticipantsGroupe;
drop table if exists ParticipantsEvenement;
drop table if exists Publication;
drop table if exists Groupe;
drop table if exists Evenement;
drop table if exists Person;

create table Person (
	idPerson INT AUTO_INCREMENT PRIMARY KEY,
	mail VARCHAR(127) UNIQUE NOT NULL,
	firstName VARCHAR(254),
	lastName VARCHAR(254),
	password VARCHAR(254) NOT NULL
) engine=innodb;

create table Evenement (
	idEvenement INT AUTO_INCREMENT PRIMARY KEY,
	nameEvenement VARCHAR(254) NOT NULL,
	description TEXT,
	lieu TEXT NOT NULL,
	moment TIMESTAMP NOT NULL,
	publicEvenement BIT(1),
	orga INT NOT NULL,
	CONSTRAINT FOREIGN KEY (orga) REFERENCES Person(idPerson)
) engine=innodb;

create table Groupe (
	idGrp INT AUTO_INCREMENT PRIMARY KEY,
	nameGrp VARCHAR(254) NOT NULL,
	leader INT NOT NULL,
	parentEvenement INT NOT NULL,
	CONSTRAINT FOREIGN KEY (leader) REFERENCES Person(idPerson),
	CONSTRAINT FOREIGN KEY (parentEvenement) REFERENCES Evenement(idEvenement)
) engine=innodb;

create table Publication (
	idPubli INT AUTO_INCREMENT PRIMARY KEY,
	evenement INT NOT NULL,
	author INT NOT NULL,
	moment TIMESTAMP NOT NULL,
	message TEXT,
	CONSTRAINT FOREIGN KEY (evenement) REFERENCES Evenement(idEvenement),
	CONSTRAINT FOREIGN KEY (author) REFERENCES Person(idPerson)
) engine=innodb;


create table ParticipantsGroupe (
	person INT,
	groupe INT,
	PRIMARY KEY(person, groupe),
	CONSTRAINT FOREIGN KEY (person) REFERENCES Person(idPerson),
	CONSTRAINT FOREIGN KEY (groupe) REFERENCES Groupe(idGrp)
) engine=innodb;


create table SamGroupe (
	sam INT,
	groupe INT,
	PRIMARY KEY(sam, groupe),
	CONSTRAINT FOREIGN KEY (sam) REFERENCES Person(idPerson),
	CONSTRAINT FOREIGN KEY (groupe) REFERENCES Groupe(idGrp)
) engine=innodb;

