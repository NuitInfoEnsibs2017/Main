drop procedure if exists CreateEvent;
drop procedure if exists AddParticipantEvent;
drop procedure if exists InscrireUser;
drop procedure if exists DefRandSam;
drop procedure if exists ModifyName;
drop function if exists CreateGroupe;
drop procedure if exists AddParticipant;

DELIMITER $$

CREATE PROCEDURE AddParticipant(groupeid int, userid int)
BEGIN
	INSERT INTO ParticipantsGroupe(person,groupe)
	VALUES (userid, groupeid);
END$$


CREATE FUNCTION CreateGroupe (nameG VARCHAR(254), userL INT(11), eventP INT(11))
RETURNS int
BEGIN 
	DECLARE ret INT;
	INSERT INTO Groupe (nameGrp, leader, parentEvenement)
	VALUES (nameG, userL, eventP);
	SELECT LAST_INSERT_ID () INTO ret;
	RETURN ret;
END$$



CREATE PROCEDURE ModifyName( idEv integer , newName varchar(254) )
BEGIN
	UPDATE Evenement
	SET Evenement.nameEvenement = newName
	WHERE Evenement.idEvenement = idEv ;
END$$



CREATE PROCEDURE DefRandSam ( idGrp integer )
BEGIN
	IF((SELECT COUNT(*) from SamGroupe where groupe=idGrp)=0) THEN
	INSERT INTO SamGroupe
	Set SamGroupe.sam = (Select person AS Sam FROM ParticipantsGroupe WHERE groupe=idGrp ORDER BY RAND() LIMIT 1), 
	SamGroupe.groupe=idGrp;
	ELSE
	UPDATE SamGroupe
	Set SamGroupe.sam = (Select person AS Sam FROM ParticipantsGroupe WHERE groupe=idGrp ORDER BY RAND() LIMIT 1)
	where SamGroupe.groupe=idGrp;
	END IF;
END$$



CREATE PROCEDURE InscrireUser ( email varchar(254) , nom varchar(254) , prenom varchar(254) , mdp varchar(254) )
BEGIN
	INSERT INTO Person( mail , lastName , firstName , password )
	VALUES ( email , nom , prenom , mdp ) ;
END$$


CREATE PROCEDURE CreateEvent( nomEv varchar(254) , idOrg integer , lieuEv varchar(254), temps timestamp, publicEv bit(1) ) 
BEGIN
	INSERT INTO Evenement( orga , nameEvenement, lieu, moment, publicEvenement )
	VALUES ( idOrg , nomEv, lieuEv, temps, publicEv );
END$$

DELIMITER ;
