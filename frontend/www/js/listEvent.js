// Code à executer au démarage
window.onload = function() {
  var url_string = window.location.href;
  var url = new URL(url_string);
  var event = url.searchParams.get("event");
  populateListEvent();
}

function populateListEvent() {
	$.ajax({
    url: '../php/Controleur.php',
    data: "requete=listEvent", // Le paramètre pour le controlleur PHP
    dataType: 'json',
    success: function(data) {
      console.log(data);
      str = "<tr><th>Nom</th><th>Lieu</th><th>Date</th></tr>";
      for (var i = 0; i < data.length; i++) {
        str += "<tr><td><a href='event.html?event="+data[i]["idEvenement"]+"'>"+data[i]["nameEvenement" ]+"</a></td>"+
              "<td>"+data[i]["lieu"]+"</td>"+
              "<td>"+data[i]["moment"]+"</td></tr>";
      }
      document.getElementById("listEvent").innerHTML = str;
  	},
    error: function(data) {
      console.log(data);
      console.log("Error");
    }
  });
}
