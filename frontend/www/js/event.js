// Code à executer au démarage
window.onload = function() {
  var url_string = window.location.href;
  var url = new URL(url_string);
  var event = url.searchParams.get("event");
  getEvent(event);
}

function getEvent(event) {
	$.ajax({
    url: '../php/Controleur.php',
    data: "requete=event&id="+event , // Le paramètre pour le controlleur PHP
    dataType: 'json',
    success: function(data) {
      if (data[0]["nameEvenement"]) {
        document.getElementById("type").innerHTML = "Public";
      }
      else {
        document.getElementById("type").innerHTML = "Privé";
      }
      document.getElementById("nom").innerHTML = data[0]["nameEvenement"];
      document.getElementById("date").innerHTML = data[0]["moment"];
      document.getElementById("adr").innerHTML = data[0]["lieu"];
      document.getElementById("desc").innerHTML = data[0]["description"];
      document.getElementById("listeGroupe").innerHTML = '<a href="ListGroup.html?event='+event+'"><input type="button" value="Liste des groupes"></a>';
  	},
    error: function(data) {
      console.log("Error");
    }
  });
}
