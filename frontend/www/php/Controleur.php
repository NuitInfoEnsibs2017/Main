<?php
  include_once("bdd.php");

  if ( isset($_GET["requete"]) ) {
    $bdd = bdd::getConnection();

    if (strcmp($_GET["requete"], "listEvent") == 0) {
      $query = "SELECT * FROM `Evenement`";

      $requete = $bdd->prepare($query);
      $requete->execute();

      echo json_encode($requete->fetchAll());
    }
    if (strcmp($_GET["requete"], "event") == 0) {
      $query = "SELECT * FROM `Evenement` WHERE idEvenement=".$_GET["id"];

      $requete = $bdd->prepare($query);
      $requete->execute();

      echo json_encode($requete->fetchAll());

    }

    if (strcmp($_GET["requete"], "listGroup") == 0) {
      $query = "SELECT * FROM Groupe WHERE parentEvenement=".$_GET["id"];

      $requete = $bdd->prepare($query);
      $requete->execute();

      echo json_encode($requete->fetchAll());

    }

    if (strcmp($_GET["requete"], "group") == 0) {
      $query = "SELECT * FROM `ParticipantsGroupe` WHERE groupe=".$_GET["id"];

      $requete = $bdd->prepare($query);
      $requete->execute();

      echo json_encode($requete->fetchAll());

    }

  } else if ( isset($_POST["data"]) ) {
    $data = $_POST["data"];
    $bdd = bdd::getConnection();

    if (strcmp($data["data"], "connexion") == 0) {
      if (isset($data["id"]) && isset($data["pass"])) {
        $query = "
        SELECT idPerson
        FROM Person
        where mail=:id
        and password=:pass
        ";

        $requete = $bdd->prepare($query);
        $requete->execute(array(
          'id' => $data["id"],
          'pass' => $data["pass"]
        ));

        echo json_encode($requete->fetchAll());

      }

    } else if (strcmp($data["data"], "inscription") == 0) {
      if (isset($data["firstname"]) && isset($data["name"]) && isset($data["id"]) && isset($data["pass"])) {

        $query = '
        insert into Person (mail, firstName, lastName, password)
        values (:mail, :firstname, :lastname, :password);

        ';

        $requete = $bdd->prepare($query);
        $requete->execute(array(
          'mail' => $data["id"],
          'password' => $data["pass"],
          'firstname' => $data["firstname"],
          'lastname' => $data["name"]
        ));

        echo "inscription";

      }
    }
  }
?>
