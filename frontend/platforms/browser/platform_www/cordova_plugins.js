cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-phonecaller/www/phonecaller.js",
        "id": "cordova-plugin-phonecaller.PhoneCaller",
        "pluginId": "cordova-plugin-phonecaller",
        "clobbers": [
            "PhoneCaller"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-phonecaller": "0.0.2"
}
// BOTTOM OF METADATA
});